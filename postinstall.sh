#!/bin/sh

if [ $# -ne 3 ]; then
    echo "Usage: <hostname> <user name> <ssh pub key>"
    exit 1
fi

hostname="$1"
user_name="$2"
ssh_pub_key="$3"

mkdir -p "/home/${user_name}/.ssh"
echo "${ssh_pub_key}" >> "/home/${user_name}/.ssh/authorized_keys"
chmod 700 "/home/${user_name}/.ssh"
chmod 600 "/home/${user_name}/.ssh/authorized_keys"
chown -R "${user_name}:${user_name}" "/home/${user_name}"

echo "${user_name} ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
echo "${hostname}" > /etc/hostname

echo 'ip: \\4' >> /etc/issue

passwd -l root
passwd -l ${user_name}

rm -rf $(readlink -f $0)
